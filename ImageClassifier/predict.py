import argparse, json, torch
import numpy as np
import matplotlib.pyplot as plt
from torch import nn, optim
from torchvision import datasets, transforms, models
from PIL import Image

    
def get_args():
    parser = argparse.ArgumentParser(
            description='Load a pre-trained image classfier model and use it to predict the flower types of an image',
            usage='python predict.py flowers/test/11/image_03151.jpg checkpoint_cmd.pth --top_k 3 --category_names cat_to_name.json --gpu'
            )
    parser.add_argument('img_path', 
                        type = str,
                        help = 'Image path that needs to be predicted')
    
    parser.add_argument('checkpoint_path', 
                        type = str,
                        help = 'Checkpoint file where the pre-trained model is saved')
     
    parser.add_argument('--top_k', 
                        dest = 'topk', 
                        default = '5',
                        help = 'Returns the top k possible classes')
    
    parser.add_argument('--category_names', 
                        dest = 'category_names',
                        type = str, 
                        default = 'cat_to_name.json',
                        help = 'Mapping file that maps categories to real names')
    
    parser.add_argument('--gpu', 
                        dest = 'gpu',
                        default = False, 
                        action = 'store_true',
                        help = 'Use GPU during the prediction')
    return parser.parse_args()


def load_checkpoint(checkpoint_path):
    checkpoint = torch.load(checkpoint_path)
    model = models.vgg16(pretrained=True)
    arch = checkpoint['arch']
    if arch == 'vgg13':
        model = models.vgg13(pretrained=True)
    
    model.class_to_idx = checkpoint['class_to_idx']
    model.classifier = checkpoint['classifier']
    model.load_state_dict(checkpoint['state_dict'])
    return model


def process_image(image):
    img = Image.open(image)
    img_transforms = transforms.Compose([transforms.Resize(256),
                                        transforms.CenterCrop(224),
                                        transforms.ToTensor(),
                                        transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
                                        ])
    processed_img = img_transforms(img)
    return processed_img


def predict(image_path, model, topk, category_names, gpu):
    with open(category_names, 'r') as f:
        cat_to_name = json.load(f)
        
    model.eval()
    if gpu:
        model.to('cuda')
        
    processed_img = process_image(image_path).unsqueeze_(0)
    if gpu:
        processed_img = processed_img.to('cuda')
        
    with torch.no_grad():
        output = model.forward(processed_img)
        ps = torch.exp(output).topk(topk)
        probs, indices = ps[0].cpu(), ps[1].cpu()
        probs = probs.numpy()[0]
        class_to_idx_inverted = {model.class_to_idx[i]: i for i in model.class_to_idx}
        classes= [class_to_idx_inverted[i] for i in indices.numpy()[0]]
        labels = [cat_to_name[i] for i in classes]
    return probs, labels


def main():
    args = get_args()
    model_trained = load_checkpoint(args.checkpoint_path)
    use_gpu = torch.cuda.is_available() and args.gpu
    probs, labels = predict(args.img_path, model_trained, args.topk, args.category_names, use_gpu)
    
    print("The image to predict is: {}".format(img_path))
    print("The loaded checkpoint is: {}".format(checkpoint_path))
    print("The predicted flower types are: {}".format(labels))
    print("The corresponding probabilies of the above flower tpyes are: {}".format(probs))

    
if __name__ == '__main__':
     main()
