import argparse, torch
from torch import nn, optim
from torchvision import datasets, transforms, models
from collections import OrderedDict
from workspace_utils import active_session


def get_args():
    parser = argparse.ArgumentParser(
            description='Train and save an image classifier.',
            usage='python train.py flowers --arch "vgg13" --learning_rate 0.001 --hidden_units 512 --epochs 20 --gpu'
            )
    parser.add_argument('data_dir', 
                        type = str,
                        help = 'Directory where training and test images are stored')
    
    parser.add_argument('--save_dir', 
                        dest = 'save_dir',
                        type = str,
                        help = 'Directory where checkpoint file is stored')
    
    supported_archs = {'vgg13',
                       'vgg16'}
    parser.add_argument('--arch', 
                        dest = 'arch', 
                        default = 'vgg16',
                        choices = supported_archs,
                        help = 'Model architecture')
    
    parser.add_argument('--hidden_units', 
                        dest = 'hidden_units',
                        type = int, 
                        default = 500,
                        help = 'Number of hidden units in the model')
    
    parser.add_argument('--learning_rate', 
                        dest = 'learning_rate',
                        type = float, 
                        default = 0.001,
                        help = 'Learning rate during the model training')
    
    parser.add_argument('--epochs',
                        dest = 'epochs',
                        type = int, 
                        default = 3,
                        help = 'Number of epochs during the model training')
    
    parser.add_argument('--gpu', 
                        dest = 'gpu',
                        default = False, 
                        action = 'store_true',
                        help = 'Use GPU during the model training')
    return parser.parse_args()


def load_data(data_dir):
    data_dir = data_dir
    train_dir = data_dir + '/train'
    valid_dir = data_dir + '/valid'
    
    train_transforms = transforms.Compose([transforms.RandomRotation(30),
                                         transforms.RandomResizedCrop(224),
                                         transforms.RandomHorizontalFlip(),
                                         transforms.ToTensor(),
                                         transforms.Normalize([0.485, 0.456, 0.406],[0.229, 0.224, 0.225])])
    
    valid_transforms = transforms.Compose([transforms.Resize(256),
                                         transforms.CenterCrop(224),
                                         transforms.ToTensor(),
                                         transforms.Normalize([0.485, 0.456, 0.406],[0.229, 0.224, 0.225])])
        
    train_datasets = datasets.ImageFolder(train_dir,transform = train_transforms)
    valid_datasets = datasets.ImageFolder(valid_dir,transform = valid_transforms)

    trainloaders = torch.utils.data.DataLoader(train_datasets, batch_size=64, shuffle=True)
    validloaders = torch.utils.data.DataLoader(valid_datasets, batch_size=32)
    return train_datasets, valid_datasets, trainloaders, validloaders

    
def create_classfier(model, hidden_units):    
    for param in model.parameters():
        param.requires_grad = False
    
    num_in_features = model.classifier[0].in_features    
    classifier = nn.Sequential(OrderedDict([
                                ('fc1', nn.Linear(num_in_features, hidden_units)),
                                ('relu', nn.ReLU()),
                                ('drop1', nn.Dropout(p=0.5)),
                                ('fc2', nn.Linear(hidden_units,102)),
                                ('output', nn.LogSoftmax(dim=1))
                ]))
    model.classifier = classifier
    return model


def validation(model, validloaders, criterion, gpu):
    valid_loss = 0
    accuracy = 0
    for ii, (inputs, labels) in enumerate(validloaders):
        if gpu:
            inputs, labels = inputs.to('cuda'), labels.to('cuda')
        outputs = model.forward(inputs)
        valid_loss += criterion(outputs, labels).item()
        ps = torch.exp(outputs)
        equality = (ps.max(dim=1)[1] == labels.data)
        accuracy += equality.type(torch.FloatTensor).mean() 
    return valid_loss, accuracy        


def train_model(model, epochs, optimizer, learning_rate, trainloaders, validloaders, gpu):
    criterion = nn.NLLLoss()
    optimizer = optimizer
    
    epochs = epochs
    print_every = 40
    steps = 0
    
    if gpu:
        model.to('cuda')
    
    with active_session():
        for e in range(epochs):
            running_loss = 0
            model.train()
            
            for ii, (inputs, labels) in enumerate(trainloaders):
                if gpu:
                    inputs, labels = inputs.to('cuda'), labels.to('cuda')
                optimizer.zero_grad()
                
                outputs = model.forward(inputs)
                loss = criterion(outputs, labels)
                loss.backward()
                optimizer.step()
                
                running_loss += loss.item()
                
                if steps % print_every == 0:
                    model.eval()
                    valid_loss = 0
                    accuracy = 0
                    
                    with torch.no_grad():
                        valid_loss, accuracy = validation(model, validloaders, criterion, gpu)
                        
                        print("Epoch: {}/{}... ".format(e+1, epochs),
                              "RunningLoss: {:.4f}".format(running_loss/print_every),
                              "ValidationLoss: {:.4f}".format(valid_loss/len(validloaders)),
                              "ValidAccuracy: {:.4f}".format(accuracy/len(validloaders)))
                        
                        running_loss = 0
                        model.train()
    return model, optimizer


def save_checkpoint(model, train_datasets, arch, optimizer, checkpoint_path):
    model.class_to_idx = train_datasets.class_to_idx

    checkpoint = {'arch': arch,
                  'state_dict': model.state_dict(),
                  'class_to_idx': model.class_to_idx,
                  'classifier': model.classifier,
                  'optimizer_dict': optimizer.state_dict()
                 }

    torch.save(checkpoint, checkpoint_path)

    
def main():
    args = get_args()
    train_datasets, valid_datasets, trainloaders, validloaders = load_data(args.data_dir)
    
    model = getattr(models, args.arch)(pretrained = True)
    use_gpu = torch.cuda.is_available() and args.gpu
    
    model = create_classfier(model, args.hidden_units)
    optimizer = optim.Adam(model.classifier.parameters(), lr=args.learning_rate)
    model, optimizer = train_model(model, args.epochs, optimizer, args.learning_rate, trainloaders, validloaders, use_gpu)
    if args.save_dir:
        checkpoint_path = args.save_dir + '/checkpoint_cmd.pth'
        save_checkpoint(model, train_datasets, args.arch, optimizer, checkpoint_path)
    
    
if __name__ == '__main__':
    main()    
    